This a git repository for developping the configuration tool of several gopigo robots

Requirements
-------------

* Install Ansible

```
sudo apt install ansible

```

* Install sshpass, you must install the sshpass program to use ssh password.

```
> sudo apt install sshpass
```
