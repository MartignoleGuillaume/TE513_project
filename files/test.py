# -*- coding: utf-8 -*-
"""
Created on Sun Mar 18 14:43:12 2018

@author: ptash
"""

from gopigo import *
import time as ti
import math as m 
DPR = 360.0/64
WHEEL_RAD = 3.25 # Wheels are ~6.5 cm diameter. 
CHASS_WID = 13.5 # Chassis is ~13.5 cm wide.
SD=20

servo(0)
def bwd_cm(dist=None):
    if dist is not None:
        pulse = int(cm2pulse(dist))
        enc_tgt(1,1,pulse)
    bwd()

def left_deg(deg=None):
    if deg is not None:
        pulse= int(deg/DPR)
        enc_tgt(0,1,pulse)
    left()

def right_deg(deg=None):

    if deg is not None:
        pulse= int(deg/DPR)
        enc_tgt(1,0,pulse)
    right()

def cm2pulse(dist):
    wheel_circ = 2*math.pi*WHEEL_RAD # [cm/rev] cm traveled per revolution of wheel
    revs = dist/wheel_circ
    PPR = 18 # [p/rev] encoder Pulses Per wheel Revolution
    pulses = PPR*revs # [p] encoder pulses required to move dist cm.
    return pulses

def turn_to(angle):

    servo(90)
    degs = angle-90
    if (degs > 0):
        right_deg(abs(degs))
    
    if (degs < 0):
        left_deg(abs(degs))
    
def scan1():
    n=[]
    d=us_dist(15)
    for i in range (45,135,15):
        servo(i)
        d=us_dist(15)*m.sin(m.radians(i))
        n.append(d)
        if(min(n)>SD):   
            n.append(d)
        else:            
            n=[min(n)]
    return n

def scan2():
    
    A=[]
    V=[]
    for i in range (10,160,10):
        servo(i)
        d=us_dist(15)*m.sin(m.radians(i))
        A.append(i)
        V.append(d)
    a=max(V)
    n=0
    for k in range (0,len(V)):
        if(a==V[k]):
            n=n+1
            break
    ang=A[k]
        
    return(ang)
        
def rotation():
    
    d=us_dist(15)    
    print("obstacle a", d,"cm")
    A=[]
    V=[]
    for i in range (10,160,10):
        servo(i)
        d=us_dist(15)*m.sin(m.radians(i))
        A.append(i)
        V.append(d)
    a=max(V)
    b=min(V)
    
    if(b<=0.2*SD):
        bwd_cm(0.3*SD)
        ti.sleep(1)
        ang=scan2()
        
    else:
        n=0
        for k in range (1,len(V)):
            if(a==V[k] and b<=SD):
                n=n+1
                break
        ang=A[n]
    print(ang)  
   
    turn_to(ang)
    d=us_dist(15)
    if(d<=d):
        a=us_dist(15)
        if(a<2*d):
            servo(90)
            a=us_dist(15)
        else:    
            stop()
    else:
        ti.sleep(0.5)  
        stop()

    
    

def self_drive():
   
    servo(90)
    D=scan1()
    if(min(D)>SD):        
        fwd()
    else:
        stop()
        rotation()
        

servo(90)

set_speed(100)
while(True):
    self_drive()
















































